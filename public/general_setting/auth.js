angular.module("sysTest");
app.factory('Auth', function ($window) {
    var user;
    return {
        setUser: function (userId, token) {
            $window.localStorage.setItem('userId', userId);
            $window.localStorage.setItem('token', token);
        },

        isToken: function () {
            return ( $window.localStorage.getItem("token")) ? $window.localStorage.getItem("token") : false;
        },
    }
});
