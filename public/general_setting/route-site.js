angular.module("app");
mtsystem.config(function ($routeProvider) {
    $routeProvider
        .when("/meeting",{
            templateUrl: "angular/page/site/meeting.php",
            controller: "meetingCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'app',
                        files: ['angular/controller/site/meetingController.js']
                    }]);
                }]
            }
        })
        .when("/room",{
            templateUrl: "angular/page/admin/room.php",
            controller: "roomCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'app',
                        files: ['angular/controller/admin/roomController.js']
                    }]);
                }]
            }
        })

        .when("/profile",{
            templateUrl: "angular/page/admin/profile.php",
            controller: "profileCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'app',
                        files: ['angular/controller/admin/profileController.js']
                    }]);
                }]
            }
        })

        .otherwise({
            redirectTo: '/meeting'
        });
});

mtsystem.controller("homeCtr" , function ($scope,$window,$location) {

    $scope.logOut = function () {
        $window.localStorage.removeItem('userId');
        $window.localStorage.removeItem('isAdmin');
        $window.localStorage.removeItem('isUser');
        window.location = "/mcslogin";
    };

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

});

mtsystem.run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
    $rootScope.$on('$routeChangeStart', function (event) {
        if (!Auth.isUser()) {
            window.location = "/";
        }
    });
}]);








