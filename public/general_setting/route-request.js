angular.module("app");
mtsystem.config(function ($routeProvider) {
    $routeProvider

        .when("/request",{
            templateUrl: "angular/page/request/request.php",
            controller: "requestCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'app',
                        files: ['angular/controller/request/requestController.js']
                    }]);
                }]
            }
        })

        .when("/check_room",{
            templateUrl: "angular/page/request/check_room.php",
            controller: "checkRoomCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'app',
                        files: ['angular/controller/request/checkRoomController.js']
                    }]);
                }]
            }
        })

        .when("/profile",{
            templateUrl: "angular/page/admin/profile.php",
            controller: "profileCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'app',
                        files: ['angular/controller/admin/profileController.js']
                    }]);
                }]
            }
        })

        .otherwise({
            redirectTo: '/request'
        });
});

mtsystem.controller("homeCtr" , function ($scope,$window,$location) {

    $scope.logOut = function () {
        $window.localStorage.removeItem('userId');
        $window.localStorage.removeItem('isAdmin');
        $window.localStorage.removeItem('isUser');
        window.location = "/mcslogin";
    };

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };


});

mtsystem.run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
    $rootScope.$on('$routeChangeStart', function (event) {
        if (!Auth.isRequest()) {
            window.location = "/";
        }
    });
}]);








