
mtsystem.service('HttpClient',function ($http) {
    this.GetUrl = function (url) {
        return  $http({
            method: "GET",
            url: url,
            headers: {"Content-Type": "application/json;charset=utf-8"}
        });
    };

    this.GetUrlById = function (url,actionKey,Id) {
        return  $http({
            method: "GET",
            url: url+"/01",
            headers: {"Content-Type": "application/json;charset=utf-8","Authorization":actionKey,"Authid":Id}
        });
    };

    this.PostUrl = function (url,actionKey,datas) {
        return $http({
            method: "POST",
            url: url,
            data:datas,
            headers: {"Content-Type": "application/json;charset=utf-8","Authorization": actionKey}
        });
    };

    this.PutUrl = function (url,actionKey,datas,Id) {
        return $http({
            method: "PUT",
            url: url+"/01",
            data:datas,
            headers: {"Content-Type": "application/json;charset=utf-8","Authorization":actionKey, 'Authid':Id}
        });
    };

     this.DeleteUrlById = function (url,actionKey,Id) {
        return  $http({
            method: "DELETE",
            url: url+"/01",
            headers: {"Content-Type": "application/json;charset=utf-8","Authorization":actionKey,"Authid":Id}
        });
    };
});


