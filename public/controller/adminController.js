angular.module("sysTest");
app.controller('adminCtr', function($scope , $http, $window) {
    $scope.userId = $window.localStorage.getItem('userId');
    $scope.token = $window.localStorage.getItem('token');
    $scope.url = 'http://127.0.0.1:8000/api/';
    $scope.header =  {headers: {'Authorization': 'Bearer '+ $scope.token, 'Accept' : 'application/json'}};
    $scope.Page = false;

    //Accept: application/vnd.api+json

    $scope.listPatients = [];
    $scope.postPatient = {};
    $scope.postEditPat = {};
    $scope.patientId = 0;
    $scope.postData = {};
    $scope.limit = 10;
    $scope.page = 0;
    $scope.totalItem = 0;
    $scope.from = 0;

    $scope.setPagination = function(page){
        $scope.getListPatients(page)
    }

    $scope.getListPatients = function (page) {
        $scope.postData['limit'] = $scope.limit;
        $scope.postData['page'] = page;
        $scope.page = $scope.postData['current_page'];
        $http.post($scope.url + 'listPatients?page='+ $scope.page, $scope.postData ,$scope.header)
        .then(function(response) {
            $scope.listPatients = response.data.data;
            $scope.totalItem = response.data.total;
            $scope.page = response.data.current_page;
            //$scope.i = (response.data.current_page - 1)*response.data.per_page + 1;
            $scope.from = response.data.from;

        });
    }

    $scope.saveDataPatient = function(){
        $scope.postPatient['user_id'] = $scope.userId;
        $http.post($scope.url + 'patients', $scope.postPatient , $scope.header)
        .then(function(response) {
            if(response.data.id){
                $scope.getListPatients(1)
                $scope.Page = false;
            }else{
                console.log("Not insert Data!");
            }
        });
    };

    $scope.goToEditDataPatient = function(patientId){
        $scope.Page = 2;
        $http.get($scope.url + 'patients/' + patientId , $scope.header)
        .then(function(response) {
           if(response.data){
               $scope.postEditPat = response.data;
           }
        });
    }

    $scope.updatePatient = function(){
        $http.put($scope.url + 'patients/' + $scope.postEditPat['id'], $scope.postEditPat , $scope.header)
        .then(function(response) {
            if(response.data.id){
                $scope.getListPatients(1);
                alert("Update Successful!");
            }else{
                console.log("Not Update Data!");
            }
        });
    }

    $scope.goTaAddPat = function(){
        $scope.Page = 1;
        $scope.postPatient = {};
    };

    $scope.goToBack = function(){
        $scope.Page = 0;
    };

    $scope.goToDelete = function(patientId){
        $http.delete($scope.url + 'patients/' + patientId , $scope.header)
        .then(function (response) {
            if(response.data){
                $scope.getListPatients(1);
                alert("Delete item successful !");
            }
        });
    }

    $scope.getListPatients($scope.page);
});
