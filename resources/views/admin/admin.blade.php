<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .mar-top {
            margin-top: 150px !important;
        }

        .mar-b {
            margin-bottom: 10px;
        }

        .p-20 {
            padding: 20px;
        }

        .pagination>li>a, .pagination>li>span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .pagination>.active>a, .pagination>.active>a:focus,
        .pagination>.active>a:hover, .pagination>.active>span,
        .pagination>.active>span:focus, .pagination>.active>span:hover {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .pagination>.disabled>a, .pagination>.disabled>a:focus,
        .pagination>.disabled>a:hover, .pagination>.disabled>span,
        .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
            color: #777;
            cursor: not-allowed;
            background-color: #fff;
            border-color: #ddd;
        }

        .pagination>li:last-child>a, .pagination>li:last-child>span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }
        .pagination>li:first-child>a, .pagination>li:first-child>span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }
    </style>
</head>

<body ng-app="sysTest">
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" ng-controller="logCtr">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            PATIENT lIST
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href=""  ng-click="logOut()">{{ __('Logout') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container p-20" ng-controller="adminCtr">
    <div class="row justify-content-center" ng-switch="Page" >
        <div class="col-md-12">
            <div ng-switch-default>
                <div class="float-right mar-b">
                    <a class="btn btn-success text-white"
                       ng-click="goTaAddPat()">Add New</a>
                </div>
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Dob</th>
                        <th scope="col">Phone</th>
                        <th scope="col" class="text-right">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="(key, item) in listPatients">
                        <td scope="row">@{{key + from}}</td>
                        <td>@{{item.first_name}}</td>
                        <td>@{{item.last_name}}</td>
                        <td>@{{item.dob}}</td>
                        <td>@{{item.phone}}</td>
                        <td class="text-right">
                            <a class="btn btn-outline-primary btn-sm"
                               ng-click="goToEditDataPatient(item.id)">Edit</a>
                            <a class="btn btn-outline-danger btn-sm"
                               ng-click="goToDelete(item.id)">Delete</a>
                        </td>

                    </tr>
                    </tbody>
                </table>

                <div class="float-right" ng-hide="totalItem < limit">
                    <paging
                        page="1"
                        page-size="limit"
                        total="totalItem"
                        text-first="first"
                        text-last="last"
                        text-next="next"
                        text-prev="prev"
                        text-title-page="Page {page}"
                        show-prev-next="true"
                        show-first-last="true"
                        paging-action="setPagination(page)"
                    >
                    </paging>
                </div>



            </div>
            <div ng-switch-when="1">
                <form name="formPatient">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control input-sm" ng-model="postPatient.first_name">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" ng-model="postPatient.last_name">
                    </div>
                    <div class="form-group">
                        <label>DOB</label>
                        <input type="text" class="form-control" ng-model="postPatient.dob">
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" ng-model="postPatient.phone">
                    </div>

                    <footer>
                        <div class="float-right">
                            <a type="submit" class="btn btn-primary text-white" ng-click="saveDataPatient()">Save</a>
                            <a type="submit" class="btn btn-dark text-white" ng-click="goToBack()">Back</a>
                        </div>

                    </footer>
                </form>
            </div>
            <div ng-switch-when="2">
                <form name="editPatient">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control input-sm" ng-model="postEditPat.first_name">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" ng-model="postEditPat.last_name">
                    </div>
                    <div class="form-group">
                        <label>DOB</label>
                        <input type="text" class="form-control" ng-model="postEditPat.dob">
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" ng-model="postEditPat.phone">
                    </div>

                    <footer>
                        <div class="float-right">
                            <a type="submit" class="btn btn-primary text-white" ng-click="updatePatient()">Save</a>
                            <a type="submit" class="btn btn-dark text-white" ng-click="goToBack()">Back</a>
                        </div>

                    </footer>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               Are you sure that you want to delete this item?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" ng-click="deletePatient()">Yes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="addPatient">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <a class="btn btn-primary text-white" ng-click="saveData()">Save</a>
                <h5 class="modal-title">Infomation Patient</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form name="formPatient">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control input-sm" ng-model="postPatient.first_name">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" ng-model="postPatient.last_name">
                    </div>
                    <div class="form-group">
                        <label>DOB</label>
                        <input type="text" class="form-control" ng-model="postPatient.dob">
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" ng-model="postPatient.phone">
                    </div>
                    <div>
                        <a class="btn btn-primary text-white" ng-click="goTaAddPat()">test</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary text-white" ng-click="saveData()">Save</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal" ng-click="goTaAddPat()">Close</button>

            </div>

        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script>
    var app = angular.module('sysTest', ["bw.paging"]);

    app.controller("logCtr" , function ($scope,$window,$location,$http,$timeout) {

        $scope.logOut = function () {
            $window.localStorage.removeItem('userId');
            $window.localStorage.removeItem('token');
            window.location = "/";
        };
    });

    app.run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
        $rootScope.$apply(function () {
            if (!Auth.isToken()) {
                window.location = "/";
            }
        });
    }]);

</script>
<script src="{{ asset('/general_setting/auth.js')}}"></script>
<script src="{{ asset('/controller/adminController.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<script src="{{ asset('/general_setting/pagination/src/paging.js')}}"></script>


</body>
</html>
