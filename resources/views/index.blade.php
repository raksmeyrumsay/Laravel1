<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
        .mar-top {
            margin-top: 150px !important;
        }

        .pad-t-150{
            padding-top:100px;
        }
    </style>
</head>
<body  ng-app="sysTest" ng-controller="contoller">
<div class="container">
    <div class="row justify-content-center pad-t-150">
        <div class="col-md-4 offset-0">
            <div class="card">
                <div class="card-header text-center"><h4><strong>Testing Project</strong></h4></div>
                <div class="card-body">
                    <form name="form">
                        @csrf
                        <div class="form-group">
                            <label class="label">
                                Email
                            </label>
                            <div>
                                <input id="email" type="email" class="form-control @error('email')
                                    is-invalid @enderror" name="email" value="{{ old('email') }}"
                                       required autocomplete="email"  ng-model="formData.email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label">Password</label>
                            <div>
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="current-password"
                                       ng-model="formData.password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary btn-block" ng-click="doLogin()">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script>
    var app = angular.module('sysTest', []);
    app.controller('contoller', function($scope , $http, $window, Auth) {
        $scope.firstName= "John";
        $scope.lastName= "Doe";
        $scope.title = 'Login Sys';
        $scope.url = 'http://127.0.0.1:8000/api/';
        $scope.formData = {};
        $scope.doLogin = function(){
            $http.post($scope.url + "login",  $scope.formData)
                .then(function(response) {
                    //console.log(response.data);
                    if(response.data.success){
                        Auth.setUser(response.data.user.id,response.data.token);
                        $window.location.href = '/admin/';
                    }else{
                        console.log("Cannot find!")
                    }
                });
        };
    });
</script>


<script src="{{ asset('/general_setting/auth.js')}}"></script>


</body>
</html>
