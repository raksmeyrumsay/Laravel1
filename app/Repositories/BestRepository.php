<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;


class BestRepository implements BestRepositoryInterface
{

    protected $model;


    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    public function all()
    {
        return $this->model->all();
    }


    public function create(array $data)
    {
        return $this->model->create($data);
    }


    public function update(array $data, $id)
    {
        $record = $this->find($id);
        return $record->update($data);
    }


    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function show($id)
    {
        return $this->model->findOrFail($id);
    }


    public function getModel()
    {
        return $this->model;
    }


    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }


    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function paginate($limit){

        //$data =  $this->model->all();
        return $this->model->paginate($limit);
    }
}
