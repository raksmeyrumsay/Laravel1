<?php

namespace App\Repositories\Patient;

use Illuminate\Support\ServiceProvider;


class PatientServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Patient\PatientInterface', 'App\Repositories\Patient\PatientRepository');
    }
}
