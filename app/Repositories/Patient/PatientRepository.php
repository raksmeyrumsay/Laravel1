<?php

namespace App\Repositories\Patient;

use App\Patient;


class PatientRepository
{
    public $patient;

    function __construct(Patient $patient) {
        $this->patient = $patient;
    }


    public function getAll1()
    {
        return $this->patient->getAll2();
    }


    public function find($id)
    {
        return $this->patient->findPatient($id);
    }


    public function delete($id)
    {
        return $this->patient->deletePatient($id);
    }

}
