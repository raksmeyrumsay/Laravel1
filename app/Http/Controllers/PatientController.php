<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use App\Repositories\BestRepository;

class PatientController extends Controller
{
    protected $model;

    public function __construct(Patient $patient)
    {
        $this->model = new BestRepository($patient);
        error_log('test');
    }



    public function index()
    {
        return  $this->model->all();
    }



    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:10',
            'last_name' => 'required|max:10'
        ]);
        return $this->model->create($request->only($this->model->getModel()->fillable));

    }

    public function show($id)
    {
        return $this->model->show($id);
    }

    public function update(Request $request, $id)
    {
        // update model and only pass in the fillable fields
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return $this->model->find($id);
    }

    public function destroy($id)
    {
        return $this->model->delete($id);
    }



    public function geListPatient(Request $request){

        $limit = $request->limit;
        return $this->model->paginate($limit);
    }

}
